const mysql = require("mysql");

var mysqlconnection = mysql.createConnection({
    host:"localhost",
    user:"root",
    password:"",
    database:"gymfix",
    multipleStatements:true
});


mysqlconnection.connect((err) => {
    if(!err)
    {
        console.log("connected");

    }
    else
    {

        console.log("connection failed" );

    }
});

module.exports = mysqlconnection;
