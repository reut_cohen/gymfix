const express = require("express");
const cors = require("cors");

var app = express();
app.use(cors());

const bodyParser = require("body-parser");

app.use(bodyParser.json({limit: '10mb', extended: true}))


const peopleRoutes = require("./api");



app.use(bodyParser.json());
app.use("/table", peopleRoutes);


app.listen(3000);