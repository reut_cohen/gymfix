const express = require("express");
const Router = express.Router();
const mysqlConnection = require("./connection");
const {add_pose_to_user_table, get_all_from_model,get_all_from_user} = require('../dal/dao');

//get from model
Router.get("/getModel",(req,res)=>{

get_all_from_model(res);

});


 Router.post ("/insert",(req,res)=>{

insert_into_model_table(req.body,res);

});

Router.get("/getUser",(req,res)=>{

    get_all_from_user(res);
    
    });

module.exports = Router;