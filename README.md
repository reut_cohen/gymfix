# gymFix

gymFix is an algorithm that uses a camera and detect the body movements of the user while working out. gymFix will give a real-time feedback about the movement and determine whether the exercise was performed correctly or not.



the project uses p5 technology. for more details you can visit on their website : https://p5js.org/ .

 we use p5 code of pose estimation for training our model to know the correct way to exercise.
 you can easily view a live pose-estimation by using p5 browser:

 https://editor.p5js.org/kylemcdonald/sketches/H1OoUd9h7


## Installation

for this project you will need to download:

-Visual Studio Code <br />
p5 <br />
ml5 <br />

open our project in Visual Studio Code.
for testing our model, go to the exercise you want and open it on browser.

for example,
client -> Deploy -> shoulders -> shoulderPress -> right click on index.html -> open with live server.

now you can stand in front of the camera and start working out!



## Future plans

in the future, when our DB will be completed, you will need to download:

-mySQL workbench <br />
-node <br />
-GitBash<br />

and the dependencies:

```bash
npm install mysql express cors body-parser nodemon axios 
```


## Visuals

here's an example of how gymFix should work :

![Alt Text](https://media.giphy.com/media/ZBVGNlphz4AWOj7MXV/giphy.gif)






## License
[P5](https://github.com/processing/p5.js/blob/master/license.txt)


