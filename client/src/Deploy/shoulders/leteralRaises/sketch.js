let video;
let poseNet;
let pose;
let skeleton;
let words = ["ready?", "3", "2", "1", "GO!"];

let count = 0;
let flag = false;
let workOutSeconds = 15;
let counter;
let workOutTimer;
let rest = 5;
let restTimer;
let brain;
let poseLabel = "Y";

function restTime() {
  rest--;
  if (rest === 0) {
    clearInterval(restTimer);
    workOutSeconds = 15;
    workOutTimer = setInterval(workout, 1000);
  }
}
function restFunc() {
  rest = 5;
  restTimer = setInterval(restTime, 1000);
}

function startWorkout() {
  flag = true;
  counter = setInterval(timer, 1000);
}
function timer() {
  count++;
  if (count === 5) {
    clearInterval(counter);
    workOutTimer = setInterval(workout, 1000);
  }
}
function workout() {
  workOutSeconds--;
  if (workOutSeconds === 0) {
    clearInterval(workOutTimer);

    restFunc();
  }
}

function resetWorkout() {
  if (workOutSeconds === 0) {
    count = 0;
    workOutSeconds = 15;
    startWorkout();
  } else workOutSeconds = 15;
}

function setup() {
  var cnv = createCanvas(640, 480);
  cnv.position(100, 100);
  start = createButton("start");
  start.position(100, 532);
  start.mouseClicked(startWorkout);
  reset = createButton("reset");
  reset.position(200, 532);
  reset.mouseClicked(resetWorkout);
  video = createCapture(VIDEO);
  video.hide();
  poseNet = ml5.poseNet(video, modelLoaded);
  poseNet.on("pose", gotPoses);

  let options = {
    inputs: 34,
    outputs: 2,
    task: "classification",
    debug: true,
  };
  brain = ml5.neuralNetwork(options);
  const modelInfo = {
    model: "raises2/model.json",
    metadata: "raises2/model_meta.json",
    weights: "raises2/model.weights.bin",
  };
  brain.load(modelInfo, brainLoaded);
}

function brainLoaded() {
  console.log("pose classification ready!");
  classifyPose();
}

function classifyPose() {
  if (pose) {
    let inputs = [];
    for (let i = 0; i < pose.keypoints.length; i++) {
      let x = pose.keypoints[i].position.x;
      let y = pose.keypoints[i].position.y;
      inputs.push(x);
      inputs.push(y);
    }
    brain.classify(inputs, gotResult);
    console.log(brain);
  } else {
    setTimeout(classifyPose, 100);
  }
}

function gotResult(error, results) {
  if (results[0].confidence > 0.55) {
    poseLabel = results[0].label.toUpperCase();
  }
  console.log(results[0]);
  classifyPose();
}

function gotPoses(poses) {
  if (poses.length > 0) {
    pose = poses[0].pose;
    skeleton = poses[0].skeleton;
  }
}

function modelLoaded() {
  console.log("poseNet ready");
}

function draw() {
  push();
  translate(video.width, 0);
  scale(-1, 1);
  image(video, 0, 0, video.width, video.height);

  if (pose) {
    for (let i = 5; i < pose.keypoints.length; i++) {
      let x = pose.keypoints[i].position.x;
      let y = pose.keypoints[i].position.y;
      fill(0);
      stroke(255);
      ellipse(x, y, 16, 16);
    }
    for (let i = 0; i < skeleton.length; i++) {
      let a = skeleton[i][0];
      let b = skeleton[i][1];
      strokeWeight(5);
      line(a.position.x, a.position.y, b.position.x, b.position.y);
      if (poseLabel === "Y") stroke(0, 255, 0);
      else stroke(255, 0, 0);
    }
  }
  pop();
  if (flag) {
    textStyle(BOLDITALIC);
    fill(0, 255, 255);
    textSize(52);
    if (count !== 5) text(words[count], 270, height / 2);

    if (count === 5) {
      text(workOutSeconds, 10, 45);
    }
    if (workOutSeconds === 0 && count === 5) {
      text("Rest", width / 2, 170);
      text(rest, width / 2, height / 2);
    }
  }
}
